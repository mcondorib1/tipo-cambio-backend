	 
	    drop table monedas if exists;

		create table monedas (
		    id integer auto_increment not null,  			
		    moneda varchar(3) not null, 
            create_at timestamp, 			
		    update_at timestamp, 
		    primary key (id)
		);
		
		   
		create table tipos_cambio (
		    id integer auto_increment not null,
			moneda_destino integer,
			moneda_origen integer,
			tipo_cambio decimal(19,2),
			create_at timestamp,
			update_at timestamp,
			primary key (id)
		);
