package com.tcs.tipocambio.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tipos_cambio")
public class TipoCambio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "moneda_origen")
	private Integer monedaOrigen;
	@Column(name = "moneda_destino")
	private Integer monedaDestino;
	@Column(name = "tipo_cambio")
	private BigDecimal tipoCambio;
	@CreationTimestamp
	private Date createAt;
	@CreationTimestamp
	private Date updateAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(Integer monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public Integer getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(Integer monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
}
