package com.tcs.tipocambio.model;

import java.math.BigDecimal;

public class TipoCambioRequest {

	private Integer monedaOrigen;
	private Integer monedaDestino;
	private BigDecimal monto;
	public Integer getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(Integer monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public Integer getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(Integer monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

}
