package com.tcs.tipocambio.model;

import java.math.BigDecimal;

public class TipoCambioResponse {
	private Integer monedaOrigen;
	private Integer monedaDestino;
	private BigDecimal tipoCambio;
	private BigDecimal monto;
	private BigDecimal montoConTipoCambio;
	
	public Integer getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(Integer monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public Integer getMonedaDestino() {
		return monedaDestino;
	}
	public void setMonedaDestino(Integer monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public BigDecimal getMonto() {
		return monto;
	}
	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}
	public BigDecimal getMontoConTipoCambio() {
		return montoConTipoCambio;
	}
	public void setMontoConTipoCambio(BigDecimal montoConTipoCambio) {
		this.montoConTipoCambio = montoConTipoCambio;
	}
}
