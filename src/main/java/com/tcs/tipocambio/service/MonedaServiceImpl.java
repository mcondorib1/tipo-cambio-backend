package com.tcs.tipocambio.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.tipocambio.model.Moneda;
import com.tcs.tipocambio.repository.MonedaRepository;

@Service
@Transactional
public class MonedaServiceImpl implements MonedaService{
    @Autowired
	private MonedaRepository monedaRepository;
	@Override
	public Moneda registrarMoneda(Moneda moneda) {
		return monedaRepository.save(moneda);
	}

	@Override
	public Moneda actualizarMoneda(Moneda moneda) {
		return monedaRepository.save(moneda);
	}

	@Override
	public List<Moneda> listarMoneda() {
		return monedaRepository.findAll();
	}

	@Override
	public Moneda listarIdMoneda(Integer id) {
		Optional<Moneda> moneda = this.monedaRepository.findById(id);
		if(moneda.isPresent()) {
			return moneda.get();
		}else {
			return null;
		}
		
	}

	@Override
	public void eliminarMoneda(Integer id) {
		monedaRepository.deleteById(id);
		
	}


}
