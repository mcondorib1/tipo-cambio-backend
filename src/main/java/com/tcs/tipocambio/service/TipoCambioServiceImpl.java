package com.tcs.tipocambio.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.tipocambio.model.TipoCambio;
import com.tcs.tipocambio.model.TipoCambioRequest;
import com.tcs.tipocambio.model.TipoCambioResponse;
import com.tcs.tipocambio.repository.TipoCambioRepository;

@Service
@Transactional
public class TipoCambioServiceImpl implements TipoCambioService{
	@Autowired
	private TipoCambioRepository tipoCambioRepository;
	
	@Override
	public TipoCambio registrarTipoCambio(TipoCambio tipoCambio) {
		return tipoCambioRepository.save(tipoCambio);
	}

	@Override
	public TipoCambio actualizarTipoCambio(TipoCambio tipoCambio) {
		return tipoCambioRepository.save(tipoCambio);
	}

	@Override
	public List<TipoCambio> listarTipoCambio() {
		return tipoCambioRepository.findAll();
	}

	@Override
	public TipoCambio listarIdTipoCambio(Integer id) {
		Optional<TipoCambio> tipoCambio = this.tipoCambioRepository.findById(id);
		if(tipoCambio.isPresent()) {
			return tipoCambio.get();
		}else {
			return null;
		}
	}

	@Override
	public void eliminarTipoCambio(Integer id) {
		tipoCambioRepository.deleteById(id);
	}

	@Override
	public TipoCambioResponse calcularTipoCambio(TipoCambioRequest tipoCambio) {
		BigDecimal montoConTipoCambio  = new BigDecimal(BigInteger.ZERO,2);
		List<TipoCambio> ltc = tipoCambioRepository.findAll();
		TipoCambioResponse tcr = new TipoCambioResponse();
		Optional<TipoCambio> tc= ltc.stream()
				              .filter(u->u.getMonedaOrigen()==tipoCambio.getMonedaOrigen())
				              .filter(u->u.getMonedaDestino()==tipoCambio.getMonedaDestino())
				              .filter(u->u.getTipoCambio().compareTo(BigDecimal.ZERO) >0)
				              .findFirst();
		if(tc.isPresent()) {
			montoConTipoCambio  = tipoCambio.getMonto().multiply(tc.get().getTipoCambio());
			tcr.setMonedaDestino(tc.get().getMonedaDestino());
		    tcr.setMonedaOrigen(tc.get().getMonedaOrigen());
		    tcr.setMonto(tipoCambio.getMonto());
		    tcr.setTipoCambio(tc.get().getTipoCambio());
			tcr.setMontoConTipoCambio(montoConTipoCambio);
			return tcr;
		}else {
			return null;
		}
	}

}
