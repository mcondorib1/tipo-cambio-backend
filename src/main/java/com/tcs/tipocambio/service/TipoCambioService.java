package com.tcs.tipocambio.service;

import java.util.List;

import com.tcs.tipocambio.model.TipoCambio;
import com.tcs.tipocambio.model.TipoCambioRequest;
import com.tcs.tipocambio.model.TipoCambioResponse;

public interface TipoCambioService {
	TipoCambio registrarTipoCambio(TipoCambio tipoCambio);
	TipoCambio actualizarTipoCambio(TipoCambio tipoCambio);
	List<TipoCambio> listarTipoCambio();
	TipoCambio listarIdTipoCambio(Integer id);
	void eliminarTipoCambio(Integer id);
	TipoCambioResponse calcularTipoCambio(TipoCambioRequest tipoCambio);
	
}
