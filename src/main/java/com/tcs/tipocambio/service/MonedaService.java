package com.tcs.tipocambio.service;

import java.util.List;

import com.tcs.tipocambio.model.Moneda;

public interface MonedaService {
	Moneda registrarMoneda(Moneda moneda);
	Moneda actualizarMoneda(Moneda moneda);
	List<Moneda> listarMoneda();
	Moneda listarIdMoneda(Integer id);
	void eliminarMoneda(Integer id);
}
