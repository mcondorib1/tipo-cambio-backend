package com.tcs.tipocambio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.tipocambio.exception.ModeloNotFoundException;
import com.tcs.tipocambio.model.TipoCambio;
import com.tcs.tipocambio.model.TipoCambioRequest;
import com.tcs.tipocambio.model.TipoCambioResponse;
import com.tcs.tipocambio.service.TipoCambioService;


@RequestMapping("/tipocambio")
@RestController
public class TipoCambioController {
	
	@Autowired
    private TipoCambioService tipoCambioService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TipoCambio>> listar(){
		return  ResponseEntity.ok().body(tipoCambioService.listarTipoCambio());
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<TipoCambio> listarId(@PathVariable("id") Integer id){
		return ResponseEntity.ok().body(this.tipoCambioService.listarIdTipoCambio(id));
	}
	
	@GetMapping(value = "/calculo",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TipoCambioResponse> calculo(@RequestBody TipoCambioRequest tipoCambio){
		return ResponseEntity.ok().body(this.tipoCambioService.calcularTipoCambio(tipoCambio));
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody TipoCambio tipoCambio){
		return ResponseEntity.ok().body(tipoCambioService.registrarTipoCambio(tipoCambio));
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@RequestBody TipoCambio tipoCambio){
		TipoCambio tc = tipoCambioService.listarIdTipoCambio(tipoCambio.getId());
		if(tc == null) {
			throw new ModeloNotFoundException("ID: " + tipoCambio.getId());
		}else {
			return ResponseEntity.ok().body(tipoCambioService.actualizarTipoCambio(tipoCambio));
		}
		
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id){
		TipoCambio tc = tipoCambioService.listarIdTipoCambio(id);
		if(tc == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}else {
			tipoCambioService.eliminarTipoCambio(id);
		}
	}

}