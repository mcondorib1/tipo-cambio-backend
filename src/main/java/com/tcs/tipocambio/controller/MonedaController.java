package com.tcs.tipocambio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcs.tipocambio.exception.ModeloNotFoundException;
import com.tcs.tipocambio.model.Moneda;
import com.tcs.tipocambio.service.MonedaService;


@RequestMapping("/moneda")
@RestController
public class MonedaController {
	
	@Autowired
    private MonedaService monedaService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Moneda>> listar(){
		return  ResponseEntity.ok().body(monedaService.listarMoneda());
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<Moneda> listarId(@PathVariable("id") Integer id){
		return ResponseEntity.ok().body(this.monedaService.listarIdMoneda(id));
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> registrar(@RequestBody Moneda moneda){
		return ResponseEntity.ok().body(monedaService.registrarMoneda(moneda));
	}
	
	@PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@RequestBody Moneda moneda){
		Moneda mon = monedaService.listarIdMoneda(moneda.getId());
		if(mon == null) {
			throw new ModeloNotFoundException("ID: " + moneda.getId());
		}else {
			return ResponseEntity.ok().body( monedaService.actualizarMoneda(moneda));
		}
		
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id){
		Moneda mon = monedaService.listarIdMoneda(id);
		if(mon == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}else {
			monedaService.eliminarMoneda(id);
		}
	}

}
