package com.tcs.tipocambio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.tipocambio.model.TipoCambio;

public interface TipoCambioRepository extends JpaRepository<TipoCambio,Integer> {

}
