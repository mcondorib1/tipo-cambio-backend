package com.tcs.tipocambio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tcs.tipocambio.model.Moneda;

public interface MonedaRepository extends JpaRepository<Moneda,Integer> {

}
